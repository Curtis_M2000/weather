package com.labo3.labo3;

import android.graphics.Bitmap;

import java.util.Date;

public class ViewOptions {
    private Bitmap image;
    private String name;
    private Date date;

    public ViewOptions(Bitmap image, String name, Date date){
        this.image = image;
        this.name = name;
        this.date = date;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }
}
