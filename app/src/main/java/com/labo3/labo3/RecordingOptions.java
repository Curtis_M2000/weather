package com.labo3.labo3;

import android.graphics.Bitmap;

public class RecordingOptions {
    private Bitmap image;
    private String name;

    public RecordingOptions(Bitmap image, String name){
        this.image = image;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Bitmap getImage() {
        return image;
    }
}
