package com.labo3.labo3;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import com.labo3.labo3.ui.login.LoginActivity;
import com.labo3.labo3.ui.main.SectionsPagerAdapter;
import java.io.IOException;
import java.util.List;

public class PrincipalActivity extends AppCompatActivity implements SensorEventListener {
    private static Context ctx;
    private Toolbar toolbar;
    public static int color;

    private SensorManager sensorManager;
    private Sensor pressure;
    private Sensor temperature;
    private Sensor luminosity;
    private Sensor humidity;

    static float tempVal, lumVal, presVal, humVal;

    private static BatteryTemperature bt;

    private static final String[] INITIAL_PERMS = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        switch (GateWay.getTheme()){
            case 1:
                getTheme().applyStyle(R.style.Theme1, true);
                color = R.style.Theme1;
                break;

            case 2:
                getTheme().applyStyle(R.style.Theme2, true);
                color = R.style.Theme2;
                break;

            case 3:
                getTheme().applyStyle(R.style.Theme3, true);
                color = R.style.Theme3;
                break;

            case 4:
                getTheme().applyStyle(R.style.Theme4, true);
                color = R.style.Theme4;
                break;
        }

        setContentView(R.layout.activity_principal);

        switch (GateWay.getTheme()){
            case 1:
                GateWay.setBackground(R.drawable.back);
                ((ImageView) findViewById(R.id.backimg)).setImageResource(R.drawable.back);
                break;

            case 2:
                GateWay.setBackground(R.drawable.back2);
                ((ImageView) findViewById(R.id.backimg)).setImageResource(R.drawable.back2);
                break;

            case 3:
                GateWay.setBackground(R.drawable.back3);
                ((ImageView) findViewById(R.id.backimg)).setImageResource(R.drawable.back3);
                break;

            case 4:
                GateWay.setBackground(R.drawable.back4);
                ((ImageView) findViewById(R.id.backimg)).setImageResource(R.drawable.back4);
                break;
        }

        requestPermissions(INITIAL_PERMS, 1337);
        ctx = this;

        toolbar = findViewById(R.id.toolbar2);
        toolbar.setTitle("WEATHER");
        setSupportActionBar(toolbar);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        TextView label = findViewById(R.id.label);
        label.setText(GateWay.getSurname().substring(0, 1).toUpperCase() + GateWay.getName().substring(0, 1).toUpperCase());

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = GateWay.getSurname() + " " + GateWay.getName();
                System.out.println(login);
                Snackbar.make(view, login, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if(GateWay.getSendState()){
            GateWay.setSendState(false);
            AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
            alert.setCancelable(false);
            alert.setTitle("Readings received");
            alert.setMessage("Your readings have successfully been sent");

            alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "CLOSE", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                    });
            alert.show();
        }

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        temperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        luminosity = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        pressure = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        //humidity = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            humidity = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY); // requires API level 14.
        }
        if (humidity == null) {
            //Toast.makeText(this, "not supported api", Toast.LENGTH_LONG).show();
        }

        bt = new BatteryTemperature();
        this.registerReceiver(this.bt, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        Sensor sensor = event.sensor;
        switch (sensor.getType()){
            case Sensor.TYPE_AMBIENT_TEMPERATURE:
                tempVal = event.values[0];
                break;

            case Sensor.TYPE_LIGHT:
                lumVal = event.values[0];
                break;

            case Sensor.TYPE_PRESSURE:
                presVal = event.values[0];
                break;

            case Sensor.TYPE_RELATIVE_HUMIDITY:
                humVal = event.values[0];
                break;
        }
    }

    @Override
    protected void onResume() {
        // Register a listener for the sensor.
        super.onResume();
        sensorManager.registerListener(this, temperature, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, luminosity, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, pressure, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(this, humidity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // Be sure to unregister the sensor when the activity pauses.
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item){
        super.onOptionsItemSelected(item);
        Intent i;
        final AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
        alert.setCancelable(false);

        switch (item.getItemId()) {
            case R.id.senddata:
                alert.setTitle("SEND INFORMATION");

                if(GateWay.collectedData.getLongitude() != null && GateWay.collectedData.getLatitude() != null && GateWay.collectedData.getTemperature() != null && GateWay.collectedData.getPressure() != null && GateWay.collectedData.getLuminosity() != null && GateWay.collectedData.getHumidity() != null){
                    alert.setMessage("Would you like to send the recorded information?");
                    alert.setButton(AlertDialog.BUTTON_POSITIVE, "SEND", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            GateWay gw = new GateWay(ctx);
                            gw.sendReadings();
                            alert.cancel();
                        }
                    });
                }
                else{
                    alert.setMessage("Record all information first before sending data.");
                }

                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.cancel();
                    }
                });
                alert.show();

                break;

            case R.id.logout:
                finish();
                i = new Intent(this, LoginActivity.class);
                startActivity(i);
                break;

            case R.id.exit:

                alert.setTitle("Exit");
                alert.setMessage("Are you sure you want to exit");

                alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                        });

                alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { finishAffinity(); }});

                alert.show();
                break;

            case Menu.FIRST + 1:
                finish();
                GateWay.setTheme(1);
                i = new Intent(this, PrincipalActivity.class);
                startActivity(i);
                break;

            case Menu.FIRST + 2:
                finish();
                GateWay.setTheme(2);
                i = new Intent(this, PrincipalActivity.class);
                startActivity(i);
                break;

            case Menu.FIRST + 3:
                finish();
                GateWay.setTheme(3);
                i = new Intent(this, PrincipalActivity.class);
                startActivity(i);
                break;

            case Menu.FIRST + 4:
                finish();
                GateWay.setTheme(4);
                i = new Intent(this, PrincipalActivity.class);
                startActivity(i);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog alert = new android.app.AlertDialog.Builder(this).create();
        alert.setCancelable(false);
        alert.setTitle("Exit");
        alert.setMessage("Are you sure you want to exit");

        alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                }
        );

        alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        alert.show();
    }

    public static void PopUpMenu1(View v, final int position, Context ct){
        PopupMenu pop = new PopupMenu(getContext(), v, Gravity.RIGHT);
        pop.getMenu().add(Menu.NONE, Menu.FIRST + 5, 1,"VIEW");
        pop.show();
        final Context cc = ct;

        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case Menu.FIRST + 5:
                        final Dialog dialPop = new Dialog(cc);
                        dialPop.setContentView(R.layout.view_choosen_data);

                        Window window = dialPop.getWindow();
                        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        dialPop.show();

                        CollectedData cd = CollectedData.getSingleData(position);
                        System.out.println(position + ".");

                        TextView name = (TextView)dialPop.findViewById(R.id.c_name);
                        name.setText("Data Collected By " + cd.getName());

                        TextView date = (TextView)dialPop.findViewById(R.id.c_date);
                        date.setText(cd.getDate());

                        TextView location = (TextView)dialPop.findViewById(R.id.c_location);
                        GpsLocationTracker gps = new GpsLocationTracker(ctx);
                        final Geocoder geocoder = new Geocoder(cc);

                        if(gps.canGetLocation) {
                            List<Address> addressList = null;

                            try {
                                addressList = geocoder.getFromLocation(Double.parseDouble(cd.getLatitude()), Double.parseDouble(cd.getLongitude()), 1);

                            } catch (IOException e) {
                                e.printStackTrace();
                                System.out.println(e);
                            }

                            location.setText(addressList.get(0).getCountryName() + ", " + addressList.get(0).getAdminArea() + ", " + addressList.get(0).getLocality());
                        }

                        else{
                            location.setText("Location not found");
                        }

                        TextView longitude = (TextView)dialPop.findViewById(R.id.c_longitude);
                        longitude.setText(cd.getLongitude() + " °");

                        TextView latitude = (TextView)dialPop.findViewById(R.id.c_latitude);
                        latitude.setText(cd.getLatitude() + " °");

                        TextView temperature = (TextView)dialPop.findViewById(R.id.c_temperature);
                        temperature.setText(cd.getTemperature() + " °C");

                        TextView luminosity = (TextView)dialPop.findViewById(R.id.c_Luminosity);
                        luminosity.setText(cd.getLuminosity() + " lx");

                        TextView pressure = (TextView)dialPop.findViewById(R.id.c_Pressure);
                        pressure.setText(cd.getPressure() + " hPa");

                        TextView humidity = (TextView)dialPop.findViewById(R.id.c_Humidity);
                        humidity.setText(cd.getHumidity() + " %");

                        Button btnC = dialPop.findViewById(R.id.c_btn);

                        btnC.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialPop.cancel();
                            }
                        });
                        break;
                }
                return true;
            }
        });
    }

    public static void PopUpMenu2(View v, final int position, final Context ctx){
        PopupMenu pop = new PopupMenu(getContext(), v, Gravity.RIGHT);
        pop.getMenu().add(Menu.NONE, Menu.FIRST + 6, 2,"VIEW DATA");
        pop.getMenu().add(Menu.NONE, Menu.FIRST + 7, 1,"RECORD NEW DATA");
        pop.show();

        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {

                    case Menu.FIRST + 7:
                        Draw drawing = null;
                        final Dialog dialPop = new Dialog(ctx);
                        Window window;

                        switch (position){
                            case 0:
                                GpsLocationTracker gps = new GpsLocationTracker(ctx);
                                final Geocoder geocoder = new Geocoder(getContext());

                                if(gps.canGetLocation){
                                    final double latitude = gps.getLatitude();
                                    final double longitude = gps.getLongitude();

                                    List<Address> addressList = null;

                                    try{
                                        addressList = geocoder.getFromLocation(latitude, longitude, 1);

                                    }
                                    catch(IOException e){
                                        e.printStackTrace();
                                        System.out.println(e);
                                    }

                                    String country = addressList.get(0).getCountryName();
                                    String region = addressList.get(0).getAdminArea();
                                    String town = addressList.get(0).getLocality();

                                    final AlertDialog alert = new android.app.AlertDialog.Builder(ctx).create();
                                    alert.setCancelable(false);
                                    alert.setTitle("LOCATION INFORMATION");
                                    alert.setMessage("Country: " + country +
                                                    "\nRegion Code: " + region+
                                                    "\nTown: " + town +
                                                    "\nLatitude: " + latitude +
                                                    "\nLongitude: " + longitude);

                                    alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                                            });

                                    alert.setButton(AlertDialog.BUTTON_POSITIVE, "SAVE", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            GateWay.collectedData.setLatitude(String.valueOf(latitude));
                                            GateWay.collectedData.setLongitude(String.valueOf(longitude));
                                            Toast.makeText(getContext(), "Location temporarily saved", Toast.LENGTH_SHORT).show();
                                            alert.cancel();
                                        }
                                    });
                                    alert.show();
                                }

                                else {
                                    gps.showSettingsAlert();
                                }

                                break;

                            case 1:
                                drawing = new Draw(ctx, bt.get_temp(), 0, 80, 10, " °C", "Thermometer", 1, dialPop);
                                dialPop.setContentView(drawing);
                                window = dialPop.getWindow();
                                window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                                window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                                dialPop.show();
                                break;

                            case 2:
                                drawing = new Draw(ctx, lumVal, 0, 100, 10, " lx", "Photometer", 2, dialPop);
                                dialPop.setContentView(drawing);
                                window = dialPop.getWindow();
                                window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                                window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                                dialPop.show();
                                break;

                            case 3:
                                drawing = new Draw(ctx, presVal, 0, 2000, 10, " hPa", "Barometer", 3, dialPop);
                                dialPop.setContentView(drawing);
                                window = dialPop.getWindow();
                                window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                                window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                                dialPop.show();
                                break;

                            case 4:
                                Toast.makeText(ctx, "No humidity sensor available", Toast.LENGTH_SHORT).show();
                                //drawing = new Draw(ctx, humVal, 0, 2000, 10, "hPa", "Hygrometer", 4, dialPop);
                                break;
                        }
                        break;

                    case Menu.FIRST + 6:
                        final AlertDialog alert = new android.app.AlertDialog.Builder(ctx).create();
                        alert.setCancelable(false);

                        switch (position){
                            case 0:
                                if(GateWay.collectedData.getLongitude() == null && GateWay.collectedData.getLatitude() == null){
                                    Toast.makeText(getContext(), "No location recorded. Yet!", Toast.LENGTH_SHORT).show();
                                }

                                else{
                                    GpsLocationTracker gps = new GpsLocationTracker(ctx);
                                    final Geocoder geocoder = new Geocoder(getContext());

                                    if(gps.canGetLocation){
                                        final double latitude = Double.parseDouble(GateWay.collectedData.getLatitude());
                                        final double longitude = Double.parseDouble(GateWay.collectedData.getLongitude());

                                        List<Address> addressList = null;

                                        try{
                                            addressList = geocoder.getFromLocation(latitude, longitude, 1);

                                        }
                                        catch(IOException e){
                                            e.printStackTrace();
                                            System.out.println(e);
                                        }

                                        String country = addressList.get(0).getCountryName();
                                        String region = addressList.get(0).getAdminArea();
                                        String town = addressList.get(0).getLocality();

                                        alert.setTitle("LOCATION INFORMATION");
                                        alert.setMessage("Country: " + country +
                                                "\nRegion Code: " + region+
                                                "\nTown: " + town +
                                                "\nLatitude: " + latitude +
                                                "\nLongitude: " + longitude);
                                    }

                                    else {
                                        gps.showSettingsAlert();
                                    }
                                }

                                break;

                            case 1:
                                if(GateWay.collectedData.getTemperature() == null){
                                    alert.setTitle("Temperature information");
                                    alert.setMessage("No temperature recorded. Yet!");
                                }

                                else{
                                    alert.setTitle("Temperature information");
                                    alert.setMessage("value: " + GateWay.collectedData.getTemperature() + " °C");

                                }
                                break;

                            case 2:
                                if(GateWay.collectedData.getLuminosity() == null){
                                    alert.setTitle("Luminosity information");
                                    alert.setMessage("No luminosity recorded. Yet!");
                                }

                                else{
                                    alert.setTitle("Luminosity information");
                                    alert.setMessage("value: " + GateWay.collectedData.getLuminosity() + " lx");
                                }
                                break;

                            case 3:
                                if(GateWay.collectedData.getPressure() == null){
                                    alert.setTitle("Pressure information");
                                    alert.setMessage("No pressure recorded. Yet!");
                                }

                                else{
                                    alert.setTitle("Pressure information");
                                    alert.setMessage("value: " + GateWay.collectedData.getPressure() + " hPa");
                                }
                                break;

                            case 4:
                                if(GateWay.collectedData.getHumidity() == null){
                                    alert.setTitle("Humidity information");
                                    alert.setMessage("No humidity recorded. Yet!");
                                }

                                else{
                                    alert.setTitle("Humidity information");
                                    alert.setMessage("value: " + GateWay.collectedData.getHumidity() + " %");
                                }
                                break;
                        }

                        alert.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                        });
                        alert.show();
                        break;
                }
                return true;
            }
        });
    }

    public static Context getContext(){
        return ctx;
    }

    public Context getContext2(){
        return this;
    }
}