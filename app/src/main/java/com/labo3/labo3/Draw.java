package com.labo3.labo3;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Toast;

import java.text.DecimalFormat;

public class Draw extends View {
    private int screenW;
    private int screenH;
    private Context MyContext;
    private Bitmap background;
    private Paint crayon;

    private float val;
    private float minVal;
    private float maxVal;
    private int range;
    private String unit;
    private String instrument;
    private int position;
    private Dialog dialog;

    public Draw(Context context, float val, float minVal, float maxVal, int range, String unit, String instrument, int position, Dialog dialog)
    {
        super(context);
        MyContext = context;
        this.val = val;
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.range = range;
        this.unit = unit;
        this.instrument = instrument;
        this.position = position;
        this.dialog = dialog;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        screenW = w;
        screenH = h;
    }

    @Override
    protected void onDraw(Canvas canvas)
    {
        background = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(),GateWay.getBackground()), screenW, screenH, false);

        crayon = new Paint();
        crayon.setAntiAlias(true);
        crayon.setColor(Color.WHITE);

        canvas.drawBitmap(background, 0, 0, null);
        canvas.drawRect(400, 200, 600, 1200, crayon);

        crayon.setColor(Color.RED);
        canvas.drawArc(325, 1170, 675, 1470, 305, 290, true, crayon);

        crayon.setColor(Color.BLACK);
        canvas.drawLine(400, 200, 400, 1200, crayon);
        canvas.drawLine(600, 200, 600, 1200, crayon);

        crayon.setStyle(Paint.Style.STROKE);
        canvas.drawArc(325, 1170, 675, 1470, 305, 290, true, crayon);
        canvas.drawArc(400, 100, 600, 300, 180, 180, true, crayon);

        crayon.setColor(Color.RED);
        crayon.setStyle(Paint.Style.FILL);
        drawTriangle(400, 1200, 200, 200, true, crayon, canvas);

        crayon.setColor(Color.WHITE);
        canvas.drawArc(400, 100, 600, 305, 180, 180, true, crayon);

        crayon.setColor(Color.RED);
        drawReading(1200, 200, 400, 600, val, maxVal, minVal, crayon, canvas);

        crayon.setColor(Color.BLACK);
        drawScale(1200, 200, minVal, maxVal, 600, range, crayon, canvas);

        setReading(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // return super.onTouchEvent(event);
        int action = event.getAction();
        int touchX = (int)event.getX();
        int touchY = (int)event.getY();

        switch (action)
        {
            case MotionEvent.ACTION_DOWN:


                break;

            case MotionEvent.ACTION_UP:
                if (touchX > 480 && touchX < 780 && touchY > 1740 && touchY < 1830) {
                    dialog.cancel();
                }

                else if (touchX > 790 && touchX < 1000 && touchY > 1740 && touchY < 1830){
                    if(position == 1){
                        GateWay.collectedData.setTemperature(String.valueOf(val));
                        Toast.makeText(MyContext, "Temperature has been temporarily saved", Toast.LENGTH_LONG).show();
                    }
                    else if(position == 2){
                        GateWay.collectedData.setLuminosity(String.valueOf(val));
                        Toast.makeText(MyContext, "Luminosity has been temporarily saved", Toast.LENGTH_LONG).show();
                    }
                    else if(position == 3){
                        GateWay.collectedData.setPressure(String.valueOf(val));
                        Toast.makeText(MyContext, "Pressure has been temporarily saved", Toast.LENGTH_LONG).show();
                    }
                    dialog.cancel();
                }
                break;
        }
        invalidate();
        return true;
    }
    private void drawTriangle(int x, int y, int width, int height, boolean inverted, Paint paint, Canvas canvas){
        Point p1 = new Point(x,y);
        int pointX = x + width/2;
        int pointY = inverted?  y + height : y - height;

        Point p2 = new Point(pointX,pointY);
        Point p3 = new Point(x+width,y);

        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(p1.x,p1.y);
        path.lineTo(p2.x,p2.y);
        path.lineTo(p3.x,p3.y);
        path.close();

        canvas.drawPath(path, paint);
    }

    private void drawScale(float bot, float top, float minVal, float maxVal, float startX, int range, Paint paint, Canvas canvas){
        DecimalFormat df = new DecimalFormat("#.#");

        Paint paintText = paint;
        paintText.setTextSize(30);

        float scaleDiff = (float)((bot - top) / range);
        float scaleVal = bot;
        float dataDiff = (float)((maxVal-minVal) / range);
        float val = minVal;

        for(int i = 0; i <= range; i++){
            canvas.drawLine(startX, scaleVal, startX - 60, scaleVal, paint);
            canvas.drawText(String.valueOf(df.format(val)), startX + 10, scaleVal, paintText);

            for(int j=0; j<10 && i+1<=range; j++){
                if(j==4){
                    canvas.drawLine(startX, scaleVal - (float)(scaleDiff*(j+1)/10), startX - 40, scaleVal - (float)(scaleDiff*(j+1)/10), paint);
                }
                canvas.drawLine(startX, scaleVal - (float)(scaleDiff*(j+1)/10), startX - 20, scaleVal - (float)(scaleDiff*(j+1)/10), paint);
            }

            scaleVal -= scaleDiff;
            val += dataDiff;
        }
    }

    public void drawReading(float bot, float top, float left, float right, float val, float maxVal, float minVal, Paint paint, Canvas canvas){
        float percentage = (float)( val/(maxVal-minVal));
        float remove = (float)( (bot-top) *percentage );

        canvas.drawRect(left, bot - remove, right, bot, paint);
    }

    public void setReading(Canvas canvas){
        DecimalFormat df = new DecimalFormat("#.##");

        Paint paint = new Paint();
        paint.setAntiAlias(true);

        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize(53);
        paint.setColor(Color.RED);
        canvas.drawText(df.format(val) + unit, 690, 450, paint);

        paint.setColor(Color.BLACK);
        paint.setTextSize(60);
        canvas.drawText(instrument, 20, 75, paint);
        canvas.drawLine(15, 95, 400, 95, paint);

        paint.setTextSize(30);
        canvas.drawText(unit, 600, 130, paint);

        paint.setTextSize(50);
        paint.setColor(Color.RED);

        canvas.drawText("CANCEL", 540, 1800, paint);
        canvas.drawText("SAVE", 810, 1800, paint);
    }
}
