package com.labo3.labo3.ui.main;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.labo3.labo3.CustomAdapterReadings;
import com.labo3.labo3.CustomAdapterViews;
import com.labo3.labo3.GateWay;
import com.labo3.labo3.PrincipalActivity;
import com.labo3.labo3.R;
import com.labo3.labo3.RecordingOptions;
import com.labo3.labo3.ViewOptions;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";

    private PageViewModel pageViewModel;

    public static PlaceholderFragment newInstance(int index) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_SECTION_NUMBER, index);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);

        pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
        int index = 1;

        if (getArguments() != null) {
            index = getArguments().getInt(ARG_SECTION_NUMBER);
        }
        pageViewModel.setIndex(index);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = null;

        switch (getArguments().getInt(ARG_SECTION_NUMBER))
        {
            case 1:
                root = inflater.inflate(R.layout.fragment_principal, container, false);

                final ListView list = (ListView) root.findViewById(R.id.list);
                ArrayList<RecordingOptions> arrayList = new ArrayList<>();

                arrayList.add(new RecordingOptions(convertImg(this.getResources().getDrawable(R.drawable.iconlocation)), "Location"));
                arrayList.add(new RecordingOptions(convertImg(this.getResources().getDrawable(R.drawable.icontemperature )), "Temperature"));
                arrayList.add(new RecordingOptions(convertImg(this.getResources().getDrawable(R.drawable.iconluminousity)), "Luminosity"));
                arrayList.add(new RecordingOptions(convertImg(this.getResources().getDrawable(R.drawable.iconpressure)), "Pressure"));
                arrayList.add(new RecordingOptions(convertImg(this.getResources().getDrawable(R.drawable.iconhumidity)), "Humidity"));

                CustomAdapterReadings customAdapter = new CustomAdapterReadings(PrincipalActivity.getContext(), arrayList);
                list.setAdapter(customAdapter);
                break;

            case 2:
                root = inflater.inflate(R.layout.fragment_2, container, false);
                //final ListView list2 = (ListView) root.findViewById(R.id.list2);
                GateWay gw = new GateWay(PrincipalActivity.getContext());

                gw.getReadings(root);
                break;
        }

        setHasOptionsMenu(true);
        return root;
    }

    public static Bitmap convertImg(Drawable drawable){
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public Bitmap getBitMap(String imgUrl) {
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.mainmenu, menu);
        SubMenu subMenu = menu.addSubMenu(Menu.NONE, Menu.FIRST, 102,"CHANGE THEME");
        subMenu.add(Menu.NONE, Menu.FIRST + 1, 201,"ORANGE");
        subMenu.add(Menu.NONE, Menu.FIRST + 2, 202,"BLUE");
        subMenu.add(Menu.NONE, Menu.FIRST + 3, 203,"GREEN");
        subMenu.add(Menu.NONE, Menu.FIRST + 4, 204,"DARK");
        super.onCreateOptionsMenu(menu, inflater);
    }
}