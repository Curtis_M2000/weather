package com.labo3.labo3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

public class BatteryTemperature extends BroadcastReceiver {
    float temp = 0;

    float get_temp(){
        return (float)(temp / 10);
    }

    @Override
    public void onReceive(Context arg0, Intent intent) {
        temp = (float)intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE,20);
    }
}
