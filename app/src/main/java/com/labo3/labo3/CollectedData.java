package com.labo3.labo3;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CollectedData {
    private String longitude;
    private String latitude;
    private String temperature;
    private String luminosity;
    private String pressure;
    private String humidity;
    private Date date;
    private String name;

    private static ArrayList<CollectedData> data = new ArrayList<CollectedData>();
    SimpleDateFormat sdf = new SimpleDateFormat("EEE dd MMM yyyy  HH:mm:ss");

    public CollectedData(String longitude, String latitude, String temperature, String luminosity, String pressure, String humidity, Date date, String name){
        this.longitude = longitude;
        this.latitude = latitude;
        this.temperature = temperature;
        this.luminosity = luminosity;
        this.pressure = pressure;
        this.humidity = humidity;
        this.date = date;
        this.name = name;
    }

    public String getDate() { return sdf.format(date); }

    public String getHumidity() {
        return humidity;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLuminosity() {
        return luminosity;
    }

    public String getPressure() {
        return pressure;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getName() { return name; }


    public void setHumidity(String humidity) { this.humidity = humidity; }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLuminosity(String luminosity) {
        this.luminosity = luminosity;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setDate(Date date) { this.date = date; }

    public void setName(String name) { this.name = name; }


    public static ArrayList<CollectedData> getData() {
        return data;
    }

    public static void addData(CollectedData myData){
        data.add(myData);
    }

    public static CollectedData getSingleData(int pos){
        return data.get(pos);
    }

    public static void emptyReadings(){
        data.clear();
    }
}
