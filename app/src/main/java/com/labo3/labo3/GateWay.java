package com.labo3.labo3;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class GateWay {
    private String msgLogin = "";
    private Context ctx;

    private static String name = "";
    private static String surname = "";
    private static String user = "";
    private static boolean sendState = false;

    private static int theme = 1;
    private static int background = R.drawable.back;
    public static CollectedData collectedData = new CollectedData(null, null, null, null, null, "0", null, null);

    private int action = 0;
    private String msgSend = "";

    private ListView list2 = null;

    ArrayList<ViewOptions> arrayList2 = new ArrayList<>();

    public GateWay(Context ctx){
        this.ctx = ctx;
    }

    public String login(String login, String password){
        action = 1;
        dbWorker back = new dbWorker();
        back.execute(1, login, password);
        return msgLogin;
    }

    public void getReadings(View root){
        action = 2;

        list2 = (ListView) root.findViewById(R.id.list2);
        dbWorker back = new dbWorker();
        back.execute(2);
    }

    public void sendReadings(){
        action = 3;
        dbWorker back = new dbWorker();
        back.execute(3);
    }

    public class dbWorker extends AsyncTask {
        ProgressDialog progress;

        @Override
        protected void onPreExecute(){
            switch (action){
                case 1:
                    progress = new ProgressDialog(ctx);
                    progress.setCancelable(false);
                    progress.setIndeterminate(false);
                    progress.setMessage("Loading...");

                    progress.show();
                    break;

                case 2:
                    System.out.println("case 2");
                    break;
            }
        }

        @Override
        protected String doInBackground(Object[] param){
            String finalMsg = "";
            String cible = "";

            switch ((int)param[0]){
                case 1:
                    action = 1;
                    cible = "https://androidcurtis.000webhostapp.com/weather/login.php";

                    try {
                        URL url = new URL(cible);
                        HttpURLConnection con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("user", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8") +
                                "&" + URLEncoder.encode("pw", "utf-8") + "="
                                + URLEncoder.encode((String)param[2], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");

                            finalMsg = sbuff.toString();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case 2:
                    action = 2;
                    cible = "https://androidcurtis.000webhostapp.com/weather/getreadings.php";
                    CollectedData myData;

                    try {
                        URL url = new URL(cible);
                        HttpURLConnection con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");

                            finalMsg = sbuff.toString();
                        }

                        String[] result = finalMsg.split("@");

                        for(String s : result){
                            if(s.contains("#")){
                                String[] data = s.split("#");
                                arrayList2.add(new ViewOptions(getBitMap(data[2]), data[1] + " " + data[0], convertToDate(data[9])));

                                myData = new CollectedData(data[3], data[4], data[5], data[8], data[6], data[7], convertToDate(data[9]), data[1] + " " + data[0]);
                                CollectedData.addData(myData);
                            }
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case 3:
                    action = 3;
                    cible = "https://androidcurtis.000webhostapp.com/weather/insertreading.php";


                    try {
                        URL url = new URL(cible);
                        HttpURLConnection con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("user", "utf-8") + "="
                                + URLEncoder.encode(GateWay.getLogin(), "utf-8") +
                                "&" + URLEncoder.encode("longitude", "utf-8") + "="
                                + URLEncoder.encode(GateWay.collectedData.getLongitude(), "utf-8") +
                                "&" + URLEncoder.encode("latitude", "utf-8") + "="
                                + URLEncoder.encode(GateWay.collectedData.getLatitude(), "utf-8") +
                                "&" + URLEncoder.encode("temperature", "utf-8") + "="
                                + URLEncoder.encode(GateWay.collectedData.getTemperature(), "utf-8") +
                                "&" + URLEncoder.encode("pressure", "utf-8") + "="
                                + URLEncoder.encode(GateWay.collectedData.getPressure(), "utf-8") +
                                "&" + URLEncoder.encode("luminosity", "utf-8") + "="
                                + URLEncoder.encode(GateWay.collectedData.getLuminosity(), "utf-8") +
                                "&" + URLEncoder.encode("humidity", "utf-8") + "="
                                + URLEncoder.encode(GateWay.collectedData.getHumidity(), "utf-8") +
                                "&" + URLEncoder.encode("date", "utf-8") + "="
                                + URLEncoder.encode(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date()), "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");

                            finalMsg = sbuff.toString();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }

            return finalMsg;
        }

        @Override
        protected void onPostExecute(Object o){
            switch (action){
                case 1:
                    progress.cancel();
                    msgLogin = (String)o;
                    msgLogin = msgLogin.replace("\n", "");

                    AlertDialog alert;
                    alert = new AlertDialog.Builder(ctx).create();

                    if(msgLogin.contains("#")){
                        String [] data = msgLogin.split("#");
                        user = data[0];
                        name = data[1];
                        surname = data[2];

                        Intent i = new Intent(ctx, PrincipalActivity.class);
                        ctx.startActivity(i);
                    }

                    else if(msgLogin.equals("")){
                        alert.setTitle("Wrong login");
                        alert.setMessage("Entered login and/or password incorrect");
                        alert.show();
                    }

                    else{
                        alert.setTitle("Login error");
                        alert.setMessage("There was a problem why trying to login. Try again!");
                        alert.show();
                    }
                    break;

                case 2:
                    CustomAdapterViews customAdapter2 = new CustomAdapterViews(PrincipalActivity.getContext(), arrayList2);
                    list2.setAdapter(customAdapter2);
                    break;

                case 3:
                    String res = (String)o;
                    if(res.contains("goodcurtis")){
                        setSendState(true);
                        CollectedData.emptyReadings();
                        Intent i = new Intent(ctx, PrincipalActivity.class);
                        GateWay.collectedData = new CollectedData(null, null, null, null, null, "0", null, null);
                        ctx.startActivity(i);
                    }
                    else{
                        System.out.println(res);
                        Toast.makeText(ctx, "An error occurred while sending data. No Changes made!", Toast.LENGTH_LONG).show();
                    }
            }
        }
    }

    public static String getLogin() {
        return user;
    }

    public static String getName() {
        return name;
    }

    public static String getSurname() {
        return surname;
    }

    public Bitmap getBitMap(String imgUrl) {
        try {
            URL url = new URL(imgUrl);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            return null;
        }
    }

    public Date convertToDate(String text){
        String txt = text.replace("-", "/");
        Date date = null;

        try {
            date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").parse(txt);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static int getTheme() {
        return theme;
    }

    public static void setTheme(int theme) {
        GateWay.theme = theme;
    }

    public static int getBackground() {
        return background;
    }

    public static boolean getSendState(){
        return sendState;
    }

    public static void setSendState(boolean sendState) {
        GateWay.sendState = sendState;
    }

    public static void setBackground(int background) {
        GateWay.background = background;
    }
}
