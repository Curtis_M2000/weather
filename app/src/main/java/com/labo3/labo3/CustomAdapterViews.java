package com.labo3.labo3;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CustomAdapterViews implements ListAdapter {
    ArrayList<ViewOptions> arrayList;
    Context context;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

    public CustomAdapterViews(Context context, ArrayList<ViewOptions> arrayList) {
        this.arrayList = arrayList;
        this.context = context;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewOptions option = arrayList.get(position);

        if(convertView==null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.list_row_fragment_2, null);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) { }
            });

            TextView no = convertView.findViewById(R.id.position);
            TextView tittle = convertView.findViewById(R.id.title2);
            TextView theDate = convertView.findViewById(R.id.date);

            ImageView image = convertView.findViewById(R.id.list_image2);
            image.setImageBitmap(option.getImage());
            tittle.setText(option.getName());
            no.setText(String.valueOf(position+1) + "");
            theDate.setText(sdf.format(option.getDate()));

            final int pos = position;
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Animation animation1 = new AlphaAnimation(0.2f, 1.0f);
                    PrincipalActivity.PopUpMenu1(v, position, context);
                    animation1.setDuration(600);
                    v.startAnimation(animation1);
                }
            });
        }

        return convertView;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }
    @Override
    public boolean isEnabled(int position) {
        return true;
    }
    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
    }
    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }
    @Override
    public Object getItem(int position) {
        return position;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public boolean hasStableIds() {
        return false;
    }
    @Override
    public int getItemViewType(int position) {
        return position;
    }
    @Override
    public int getViewTypeCount() {
        return arrayList.size();
    }
    @Override
    public boolean isEmpty() {
        return false;
    }
}
